[![Gitter](https://badges.gitter.im/robolcd/Lobby.svg)](https://gitter.im/robolcd/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
# Summary

This is an octoprint plugin that generates a UI for Robo printer on a touch screen. The UI is built with Kivy. 

Here's current git workflow: https://github.com/Robo3D/RoboLCD/wiki/Git-Workflow 

# Installation Steps

### Option 1: Download this Raspbian image and write it to an SD card. It comes installed with RoboLCD plugin and Octoprint 
https://drive.google.com/open?id=0BwRrLPmfRAPyakw3ZVVpZk9acmc

How to write an image to SD card:
https://www.raspberrypi.org/documentation/installation/installing-images/

Note: the virtual enviroment in which all Octoprint related packages are installed is `home/pi/oprint`

### Option 2: Install it with your own instance of Octoprint. Follow the instructions below...

## 1. Prepare the system

###Install [Kivy] (https://kivy.org/docs/installation/installation-linux.html#installation-in-a-virtual-environment)


**Ubuntu** 

Install necessary system packages:

```
sudo apt-get install -y \
    python-pip \
    build-essential \
    git \
    python \
    python-dev \
    ffmpeg \
    libsdl2-dev \
    libsdl2-image-dev \
    libsdl2-mixer-dev \
    libsdl2-ttf-dev \
    libportmidi-dev \
    libswscale-dev \
    libavformat-dev \
    libavcodec-dev \
    zlib1g-dev \
    usbmount
```

*Note*: Depending on your Linux version, you may receive error messages related to the “ffmpeg” package. In this scenario, use “libav-tools ” in place of “ffmpeg ” (above), or use a PPA (as shown below):
```
- sudo add-apt-repository ppa:mc3man/trusty-media
- sudo apt-get update
- sudo apt-get install ffmpeg
```

Install kivy in the virtual environment that octoprint has been installed in.

1. Enter/Activate the virtual enviroment: ```source path/to/virtualenv/bin/activate```
 * To verify whether you are inside the virtual environment, run ```which python```. Verify that the output points to the correct python path.  
2. Install Cython dependency: ```pip install Cython```
3. Install stable version of Kivy: ```pip install kivy```


**Rasbian Jessie**

From http://kivy.org/docs/installation/installation-rpi.html:

Install necessary system packages:
```
sudo apt-get update
sudo apt-get install usbmount libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev pkg-config libgl1-mesa-dev libgles2-mesa-dev python-setuptools libgstreamer1.0-dev git-core gstreamer1.0-plugins-{bad,base,good,ugly} python-dev cython
\ gstreamer1.0-{omx,alsa}
```
Install Kivy in the same virtual environment that OctoPrint has been installed in:

1. Enter/Activate the virtual environment: ``` source path/to/virtualenv/bin/activate```
  *  To verify whether you are inside the virtual environment, run ```which python```. Verify that the output points to the correct python path. 
  *  Note: RoboPi image has installed all dependencies within the ```oprint``` virtual environment. Therefore, type in ```source ~/oprint/bin/activate``` to activate that venv.
2. Run pip install kivy: ```pip install kivy```
3. Make sure that cython is installed in the virtual environment:```pip install cython```



## 2. Install the plugin

All python commands below require the one installed in your virtual environment. 
```
git clone https://github.com/Robo3D/RoboLCD.git
cd RoboLCD
~/oprint/bin/python setup.py develop
```

### `python setup.py develop` vs `python setup.py install`
Above you can see that I ran `python setup.py develop` instead of `install`. The difference is that `develop` will not copy this repo into the python site-packages directory. Instead, it will create a link to the RoboLCD folder you cloned from github. This means that any changes you make to code inside that cloned folder will be reflected when OctoPrit is restarted. This make development more fluid. 








