# RoboLCD Changelog

##0.3.0 (2016-08-24)

###Improvements

* File and Utilities List are not controlled with buttons for precision

* Wifi status is printed to screen under See my IP Address

* Printer status screen visually enhanced. New buttons, larger extruder temp text, larger tab text etc

* More user friendly keyboard for inputting wifi password

###Bug Fixes

* Incorrect static ip when in hotspot mode

* Changed AP mode to Hotspot (confusing for users who are not familiar with word AP)

* Start wifi hotspot button blends with background_normal

* Asynchronous behavior with Start/Pause button and other devices

* Non uniform filename format

* Crashes when trying to read stl file metadata


##0.2.1 (2016-08-9)

###Improvements

* Added plugin to Software Update check

##0.2.0 (2016-08-08)

###Improvements

* Updated aesthetic direction

##0.1.3 (2016-08-05)

### Bug Fixes

* Running python setup.py install would not copy over .kv files over to installation directory

##0.1.2 (2016-08-05)

### Bug Fixes

* proper version listed in setup.py

##0.1.1 (2016-08-05)

### Bug Fixes

* Kivy app dynamicaly becomes aware of /path/to/RoboLCD/lcd/ . Fixes issue of hardcoded directory paths for custom .kv and Icons files, and kivy app's inability to find these files on different installation paths.

##0.1.0 (2016-08-05)

### Improvements

* See local file list
* Start, pause, and cancel print file
* Connect to Wifi
* Start Hotspot
* Display IP Address and Hostname
* Display QR Code
