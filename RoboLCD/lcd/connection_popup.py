from kivy.graphics import *
from .. import roboprinter
from kivy.clock import Clock
from kivy.uix.popup import Popup
from kivy.uix.modalview import ModalView


class Connection_Popup(ModalView):
    def reconnect_button(self):
        options = roboprinter.printer_instance._printer.connect()
