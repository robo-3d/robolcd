from kivy.uix.popup import Popup
from kivy.properties import StringProperty, ListProperty
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout

from kivy.uix.button import Button
from kivy.effects.scroll import ScrollEffect
from kivy.uix.scrollview import ScrollView
from kivy.uix.gridlayout import GridLayout

from kaa_command import *


class ULabel(Label):
    pass


def get_update_lists():
    """
    Get Update Log lists from file when click "updates" button.
    :return:
    """
    if os.path.isfile(UPDATE_LOG):
        file = open(UPDATE_LOG, 'rb')
        update_binary_lists = a2b_hex(file.read())
        update_lists = str(update_binary_lists).split('\n')
        Logger.info('update_lists: {}'.format(update_lists))
        file.close()
        return update_lists
    else:
        Logger.info('No Update Log File.')
        return ''


class UpdateScreen(BoxLayout):
    """
    UpdateScreen for Updates Tab.
    """

    def __init__(self, **kwargs):

        super(UpdateScreen, self).__init__(**kwargs)

        update_lists = get_update_lists()
        if len(update_lists) == 0:
            Logger.info('No Update Log File.')

        else:
            i_text = ''
            b_text = ''
            self.ids.update_grid.add_widget(ULabel(text='[b][size=35]       Improvement:[/size][/b]'))

            infos = update_version_information(update_lists)
            Logger.info('Update informations: {}'.format(infos))

            if len(infos["improve"]) != 0:
                for i_txt in infos["improve"]:
                    if str(i_txt) == ' ':
                        continue
                    i_text = '              -  ' + str(i_txt)
                    self.ids.update_grid.add_widget(ULabel(text=i_text))

                self.ids.update_grid.add_widget(ULabel(text='[b][size=35]       Bug_Fix:[/size][/b]'))

            if len(infos["bugs"]) != 0:
                for b_txt in infos["bugs"]:
                    if str(b_txt) == ' ':
                        continue
                    b_text = '              -  ' + str(b_txt)
                    self.ids.update_grid.add_widget(ULabel(text=b_text))

    def up_action(self, *args):
        Logger.info('starting up action')
        scroll_distance = 0.4
        Logger.info(self.ids.update_scroll_view.scroll_y)

        # makes sure that user cannot scroll into the negative space
        if self.ids.update_scroll_view.scroll_y + scroll_distance > 1:

            scroll_distance = 1 - self.ids.update_scroll_view.scroll_y
        self.ids.update_scroll_view.scroll_y += scroll_distance

    def down_action(self, *args):
        Logger.info('starting down action')
        scroll_distance = 0.4

        # makes sure that user cannot scroll into the negative space
        Logger.info(self.ids.update_scroll_view.scroll_y)
        if self.ids.update_scroll_view.scroll_y - scroll_distance < 0:

            scroll_distance = self.ids.update_scroll_view.scroll_y
        self.ids.update_scroll_view.scroll_y -= scroll_distance


class RunningPopup(Popup):
    pass


class ErrorPopup(Popup):
    pass


class CompletePopup(Popup):
    pass


class NoupdatePopup(Popup):
    pass


class KaaPopup(Popup):

    msg = StringProperty()
    update_commands = ListProperty()

    def __init__(self, update_commands=None, message='', file_flag=False, **kwargs):

        super(KaaPopup, self).__init__()

        self.upcommands = update_commands
        self.msg = message
        self.file_flag = file_flag

    def handle_commands(self):
        """
        Handle the shell commands depending on message.
        :return:
        """

        self.dismiss()
        error = ErrorPopup()
        complete = CompletePopup()
        runpopup = RunningPopup()

        try:
            process_thread = threading.Thread(target=self.process_commands, args=(error, complete, runpopup, self.file_flag, self.upcommands, self.msg, Logger))
            process_thread.start()
        except Exception as e:
            Logger.info("process_thread error: {}".format(e))

        runpopup.open()

    @staticmethod
    def process_commands(error, complete, runpopup, file_flag, upcommands, msg, Logger):
        """
        Handle the shell commands depending on message.
        :return:
        """
        # when update later.
        if file_flag == True:

            for update_list in upcommands:

                return_value = run_update_command(update_list, file_flag)
                if return_value == 'error':
                    error.open()
                    break

        # Run update command immediately.
        else:

            return_value = run_update_command(msg, file_flag)
            if return_value == 'error':
                error.open()

        Logger.info('run_result: {}'.format(return_value))
        runpopup.dismiss()

        if return_value != 'error':
            complete.open()

    def update_later(self):

        if self.file_flag:  # If update run from update log file, just close the Kaapopup

            self.dismiss()

        else:  # If update run from kaa message directly, It would be run below process
            self.dismiss()

            # Check if directory exists or not.
            Logger.info('Writing message to UPDATE_LOG')

            if os.path.isfile(UPDATE_LOG):

                target = open(UPDATE_LOG, 'a')

            else:

                target = open(UPDATE_LOG, 'w+')

            decode_message = self.msg.decode("utf-8").replace('\x00', '')
            target.writelines(b2a_hex(decode_message + "\n"))
            Logger.info('Complete to write message')
            target.close()