import shlex
import subprocess
import time
import os
import threading
from binascii import b2a_hex, a2b_hex
from kivy.logger import Logger

UPDATE_LOG = "/home/pi/.octoprint/logs/.update_log.log"


def run_update_command(msg, file_flag):
	"""
	When click "update" button, run message command.
	:param msg:
	:return:
	"""
	command_lines = msg.decode("utf-8").split(';')
	Logger.info('command_lines: {}'.format(command_lines))

	# Check if it is shell command or command details.
	info_status = False

	for index, command_line in enumerate(command_lines):

		if index == 0:  # Version
			continue

		if '*' == str(command_line):  # command detail
			info_status = True
			continue

		# Writing Improvement and Bug informations to update_log.log
		if info_status:
			update_version_information(command_line)
			continue

		args = shlex.split(command_line)
		# Make run commands list.
		run_args = []
		for arg in args:
			arg = arg.replace('\x00', '')
			run_args.append(arg)

		try:
			# when run "cd" command, run below command.
			if run_args[0] == 'cd':
				os.chdir(run_args[1])
				continue

			sp = subprocess.Popen(run_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

			# Wait for ending to process shell command.
			while sp.poll() is None:
				Logger.info('p.poll:{}'.format(sp.poll()))
				time.sleep(.5)
			(results, errors) = sp.communicate()

			# If Success
			if errors == '':
				pass

			# Else Error
			else:
				Logger.info('errors: {}'.format(errors))
				if "Cloning" in str(errors):
					pass
				else:
					save_update_command(msg)
					return 'error'

		except subprocess.CalledProcessError as e:
			save_update_command(msg)
			Logger.info('subprocess error: {}'.format(e))
			return 'error'

		except Exception as eu:
			save_update_command(msg)
			Logger.info('subprocess error: {}'.format(eu))
			return 'error'

	# when run commands from update_log.log, remove this command from log file.
	if file_flag:
		try:
			src = open(UPDATE_LOG, 'rb')
			datas = src.readlines()
			new_datas = []

			for data in datas:
				if str(msg) in str(a2b_hex(data)):
					continue
				new_datas.append(a2b_hex(data))

			src.close()

			tar = open(UPDATE_LOG, 'w')
			for new_data in new_datas:
				tar.writeline(b2a_hex(new_data))

			tar.close()
		except Exception as e:
			Logger.info('File Writing error: {}'.format(e))
			return 'error'

	return 'success'


def update_version_information(update_lists):
	"""
	Make Update information text
	:param msg:
	:return: text
	"""
	# Split with Improvement and bug fix.
	improvements = []
	bug_fix = []

	for index, update_list in enumerate(update_lists):
		update_lines = update_list.split(';')

		infos = ''

		# Get update information from command.
		for index, update_line in enumerate(update_lines):
			if update_line == '*':
				infos = update_lines[index + 1]
				break
			else:
				pass

		improve_bug_infos = str(infos).split(':')

		for index, info in enumerate(improve_bug_infos):
			if index == 0:
				i_str = info.split(',')
				improvements += i_str
				Logger.info('improvements i_str: {}'.format(improvements))
			elif index == 1:
				b_str = info.split(',')
				bug_fix += b_str
				Logger.info('bug_fix b_str: {}'.format(bug_fix))

	return {'improve': improvements,
	        'bugs': bug_fix}


def save_update_command(msg):
	if os.path.isfile(UPDATE_LOG):

		target = open(UPDATE_LOG, 'a')

	else:

		target = open(UPDATE_LOG, 'w+')

	decode_message = msg.decode("utf-8").replace('\x00', '')
	target.writelines(b2a_hex(decode_message + "\n"))
	Logger.info('Save Update command')
	target.close()
